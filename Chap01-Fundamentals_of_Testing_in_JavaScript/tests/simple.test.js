/**
 * We have two functions, `sum` and `subtract` ,
 * that are ready to be tested.
 *
 * Task: check if the results are the respects.
 *
 *            **BONUS**
 * Throw an error if the test fails.
 *
 *
 * Execute: Use `node lessons/simple.js` to run the test.
 */
import { test, expect } from 'jest'
import { sum, subtract } from '../math.js'

/*
 * 
*/
test('simple', () => {
  let result = sum(3, 7)
  let expected = 10
  expect(result).toEqual(expected)

})
