/**
 * We have two functions, `sum` and `subtract` ,
 * that are ready to be tested.
 *
 * Task: check if the results are the respects.
 *
 *            **BONUS**
 * Throw an error if the test fails.
 *
 *
 * Execute: Use `node lessons/simple.js` to run the test.
 */
import test from 'tape'
import { sum, subtract } from '../math.js'

let result = sum(3, 7)
let expected = 10

/**
 * Hint: there's no magic, just the most straightforward conditional statement.
 *
 * Answer: Checkout the main branch for the answer.
 */

if (result !== expected) {
  throw new Error(`${result} is not equal to ${expected}`)
}

result = subtract(4, 3)
expected = 1

if (result !== expected) {
  throw new Error(`${result} is not equal to ${expected}`)
}

/*
 * Using tape now
*/
test('simple', t => {
  t.plan(2)

  let result = sum(3, 7)
  let expected = 10
  t.equal(result, expected)

  t.equal(result, 2)
})
