import assert from 'assert'
import { thumbWar } from '../thumb-war.js'
import { getWinner } from '../utils.js'

jest.mock('../utils', () => {
  return {
    getWinner: jest.fn((p1, p2) => {
      console.log({p1, p2})
      return p1
    })
  }
})

const winner = thumbWar('1', '2')
assert.strictEqual(winner, '1')
